<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('logout', 'Auth\LoginController@logout'); //Override logout to use get request
Route::get('/', 'HomeController@index')->name('home');
Route::get('/products/category/{id}', 'ProductCategoryController@products')->name('product.category');
Route::get('/product/{id}', 'ProductController@view')->name('product.view');
Route::get('/cart', 'CartController@cart')->name('cart');
Route::get('/checkout', 'CartController@checkout')->middleware('auth')->name('checkout');

/*
|--------------------------------------------------------------------------
  Payment Routes
|--------------------------------------------------------------------------
*/
Route::get('payment/store', 'PaymentController@store')->name('payment.store');
Route::get('payment/status', 'PaymentController@success')->name('payment.success');
Route::get('payment/cancel', 'PaymentController@cancel')->name('payment.cancel');
Route::match(['get','post','put'], 'payment/itn', 'PaymentController@itn')->name('payment.itn');

/*
|--------------------------------------------------------------------------
  Carting System Routes
|--------------------------------------------------------------------------
*/
Route::post('cart/add', 'CartController@add')->name('cart.add');
Route::get('cart/delete', 'CartController@deleteItem')->name('cart.delete');


/*
|--------------------------------------------------------------------------
  Ordering System Routes
|--------------------------------------------------------------------------
*/
Route::post('order/save', 'OrderController@save')->name('order.save');
Route::get('orders', 'OrderController@orders')->name('orders');
Route::get('order/{id}', 'OrderController@view')->name('order.view');

/*
|--------------------------------------------------------------------------
  Admin Routes
|--------------------------------------------------------------------------
*/
// Orders routes
Route::get('admin/users', 'UserController@index')->name('admin.users');
// Orders routes
Route::get('admin/orders', 'OrderController@orders')->name('admin.orders');
Route::get('admin/order/{id}', 'OrderController@view')->name('admin.order.view');
// Products routes
Route::get('admin/products/{active?}', 'ProductController@products')->name('admin.products');
Route::get('admin/product/create', 'ProductController@create')->name('admin.product.create');
Route::post('admin/product/create', 'ProductController@save')->name('admin.product.save');
Route::get('admin/product/{id}', 'ProductController@edit')->name('admin.product.edit');
Route::post('admin/product/{id}', 'ProductController@update')->name('admin.product.update');
Route::get('admin/product/activate/{id}', 'ProductController@activate')->name('admin.product.activate');
// Product categories routes
Route::get('admin/categories', 'ProductCategoryController@index')->name('admin.categories');
