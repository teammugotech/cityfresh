@php
    $title = "Cart";
@endphp
@extends('layouts.app')

@section('content')

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-error" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{ route('cart') }}">Cart</a></li>
        </ul>
        <div class="row">
            <div class="col-sm-12 mb-5" id="content">
                <h1>Shopping Cart</h1>
                <form enctype="multipart/form-data" method="post" action="#">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <td class="text-center">Image</td>
                                    <td class="text-left">Product Name</td>
                                    <td class="text-left">Quantity</td>
                                    <td class="text-right">Unit Price</td>
                                    <td class="text-right">Total</td>
                                    <td class="text-right">Remove</td>
                                </tr>
                            </thead>
                            <tbody>
                            @if(isset($cart))
                                @foreach($cart->items as $item)
                                    <tr class="product-row" data-id="{{ hashids()->encode($item->product->id) }}">
                                        <td class="text-center" style="width: 75px;">
                                            <a href="{{ route('product.view', hashids()->encode($item->product->id)) }}">
                                                <img class="img-thumbnail" title="{{ $item->product->name }}" alt="{{ $item->product->name }}" src="{{ asset('uploads/products/'.$item->product->image) }}">
                                            </a>
                                        </td>
                                        <td class="text-left"><a href="{{ route('product.view', hashids()->encode($item->product->id)) }}">{{ $item->product->name }}</a></td>
                                        <td class="text-left">
                                            <div class="input-group">
                                                <input type="number" data-id="{{ hashids()->encode($item->product->id) }}" class="form-control update-quantity" min="1" value="{{ $item->quantity }}" name="quantity">
                                            </div>
                                        </td>
                                        <td class="text-right">R {{ number_format($item->price, 2) }}</td>
                                        <td class="text-right">R {{ number_format(($item->price * $item->quantity), 2) }}</td>
                                        <td class="text-center">
                                             <span class="input-group-btn">
                                                <a href="JavaScript:void(0);" class="cart-remove" data-title="Are you sure you want to remove item from cart?" data-id="{{ hashids()->encode($item->product->id) }}">
                                                    <i class="fa fa-trash-o" style="color: #ff0000"></i>
                                                </a>
                                            </span>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <div class="alert alert-info">Your cart is empty, add some items <a class="" href="{{ route('home') }}">here!</a></div>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </form>

                <br>
                @if(isset($cart))
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-8">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td class="text-right"><strong>Sub-Total:</strong></td>
                                    <td class="text-right">R {{ number_format($cart->items_total, 2) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Delivery Fee:</strong></td>
                                    <td class="text-right">R {{ number_format($cart->delivery_fee, 2) }}</td>
                                </tr>
                                <tr>
                                    <td class="text-right"><strong>Total:</strong></td>
                                    <td class="text-right">R {{ number_format($cart->total, 2) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif
                <div class="mb-5">
                    <div class="pull-left">
                        <a class="btn btn-default" href="{{ route('home') }}">Continue Shopping</a>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('checkout') }}">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
