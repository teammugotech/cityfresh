@php
    $title = "Cart";
@endphp
@extends('layouts.app')

@section('content')

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-error" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{ route('orders') }}">My Orders</a></li>
        </ul>
        <div class="row">
            <div class="col-sm-12 mb-5" id="content">
                <h1>My Orders</h1>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td class="text-left">Date</td>
                                <td class="text-left">Contact Person</td>
                                <td class="text-left">Address</td>
                                <td class="text-right">Order Total</td>
                                <td class="text-right">Manage</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($orders as $order)
                                <tr class="product-row">
                                    <td class="text-center" style="width: 75px;">
                                        {{ \Carbon\Carbon::parse($order->created_at)->format('d-m-Y') }}
                                    </td>
                                    <td class="text-left">{{ $order->address->contact_person }}</td>
                                    <td class="text-left">{{ $order->address->address_1 . ', ' . $order->address->address_2 }}</td>
                                    <td class="text-right">{{ $order->total }}</td>
                                    <td class="text-center">
                                         <span class="">
                                            <a href="{{ route('order.view', hashids()->encode($order->id)) }}" data-toggle="tooltip" title="View order details" data-id="{{ hashids()->encode($order->id) }}">
                                                <i class="fa fa-search"></i>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="mb-5">
                    <div class="pull-left">
                        <a class="btn btn-default" href="{{ route('home') }}">Continue Shopping</a>
                    </div>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ route('checkout') }}">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
