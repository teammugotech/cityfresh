@php
    $title = "Product";
@endphp
@extends('layouts.app')

@section('content')

<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-error" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <ul class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
        <li><a href="{{ route('product.category', ['id' => hashids()->encode($product->category->id)]) }}">{{ $product->category->name }}</a></li>
        <li><a href="{{ route('product.view', hashids()->encode($product->id)) }}">{{ $product->name }}</a></li>
    </ul>
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="row">
                <div class="col-sm-6" id="image">
                    <div class="thumbnails">
                        <div>
                            <a class="thumbnail" href="#" title="{!! $product->description !!}">
                                <img src="{{ asset('uploads/products/' . $product->image) }}" title="{!! $product->description !!}" alt="{!! $product->name !!}" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h1 class="productpage-title">{{ $product->name }}</h1>
                    <ul class="list-unstyled productinfo-details-top">
                        <li>
                            <h2 class="productpage-price">{{ 'R ' . number_format($product->price, 2) }}</h2>
                        </li>
                    </ul>
                    <hr>
                    <ul class="list-unstyled product_info">
                        <li>
                            <label>Category:</label>
                            <span>
                                <a href="{{ route('product.category', ['id' => hashids()->encode($product->category->id)]) }}">{{ $product->category->name }}</a>
                            </span>
                        </li>
                        <li>
                            <label>Availability:</label>
                            <span>{{ ($product->in_stock) ? 'In Stock' : 'Out of Stock' }}</span>
                        </li>
                    </ul>
                    <hr>
                    <p class="product-desc">{!! $product->description !!}</p>
                    <div id="product">
                        <div class="form-group">
                            <label class="control-label qty-label" for="input-quantity">Quantity:</label>
                            <input type="number" name="quantity" min="1" value="{{ isset($item) ? $item->quantity : 1 }}" size="2" data-id="{{ hashids()->encode($product->id) }}" id="input-quantity" class="form-control productpage-qty" />
                            <div class="btn-group">
                                <button type="button" id="button-cart" data-loading-text="Adding..." data-id="{{ hashids()->encode($product->id) }}" class="btn btn-primary btn-lg btn-block addtocart-btn add-to-cart">Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if($relatedProducts->count())
                <h3 class="productblock-title">Related Products</h3>
                <div class="box">
                    <div id="related-slidertab" class="row owl-carousel product-slider">
                        @foreach($relatedProducts as $relatedProduct)
                            <div class="item">
                                <div class="product-thumb transition">
                                    <div class="image product-imageblock">
                                        <a href="{{ route('product.view', hashids()->encode($relatedProduct->id)) }}">
                                            <img src="{{ asset('uploads/products/'. $relatedProduct->image)  }}" alt="{{ $relatedProduct->name }}" title="{!! $relatedProduct->description !!}" class="img-responsive" />
                                        </a>
                                        <div class="button-group">
                                            <button type="button" data-id="{{ hashids()->encode($relatedProduct->id) }}" data-quantity="1" class="addtocart-btn add-to-cart instant">Add to Cart</button>
                                        </div>
                                    </div>
                                    <div class="caption product-detail">
                                        <h4 class="product-name">
                                            <a href="{{ route('product.view', hashids()->encode($relatedProduct->id)) }}" title="{{ $relatedProduct->name }}">{{ $relatedProduct->name }}</a>
                                        </h4>
                                        <p class="price product-price">
                                            <span class="price-new">{{ 'R ' . number_format($product->price, 2) }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
