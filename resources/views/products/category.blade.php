@php
    $title = "Product";
@endphp
@extends('layouts.app')

@section('content')

<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-error" role="alert">
            {{ session('error') }}
        </div>
    @endif
        <ul class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
            <li><a href="{{ route('product.category', ['id' => $category->id]) }}">{{ $category->name }}</a></li>
        </ul>
        <div class="row">
            <div id="content" class="col-sm-12">
                <h2 class="category-title">{{ $category->name }}</h2>
                <div class="row category-banner">
                    <div class="col-sm-12 category-image"><img src="/images/banners/category-banner.jpg" alt="Desktops" title="Desktops" class="img-thumbnail" /></div>
                    <div class="col-sm-12 category-desc">{!! $category->description !!}</div>
                </div>
                <div class="category-page-wrapper">
                    <div class="col-md-6 list-grid-wrapper">
                        <div class="btn-group btn-list-grid">
                            <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th hide"></i></button>
                        </div>
                    </div>
                </div>
                <br />
                <div class="grid-list-wrapper">
                    @foreach($products as $product)
                        <div class="product-layout product-list col-xs-12">
                            <div class="product-thumb">
                                <div class="image product-imageblock">
                                    <a href="{{ route('product.view', hashids()->encode($product->id)) }}"> <img src="{{ asset('uploads/products/'. $product->image) }}" alt="{{ $product->name }}" title="{{ $product->description }}" class="img-responsive" /> </a>
                                    <div class="button-group">
                                        <button type="button" class="addtocart-btn add-to-cart instant" data-quantity="1" data-id="{{ hashids()->encode($product->id) }}">Add to Cart</button>
                                    </div>
                                </div>
                                <div class="caption product-detail">
                                    <h4 class="product-name">
                                        <a href="{{ route('product.view', hashids()->encode($product->id)) }}" title="{{ $product->description }}">{{ $product->name }}</a>
                                    </h4>
                                    <p class="product-desc">{!! $product->description !!}</p>
                                    <p class="price product-price">{{ 'R ' . number_format($product->price, 2) }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
</div>
@endsection
