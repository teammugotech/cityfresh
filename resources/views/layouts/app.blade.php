<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts.partials.head')

<body>
<div class="preloader loader" style="display: block; background:#f2f2f2;"> <img src="/images/loader.gif"  alt="Loading..."/></div>
    <div id="app">
        @include('layouts.partials.header')
        <nav id="menu" class="navbar">
            <div class="nav-inner container">
                <div class="navbar-header"><span id="category" class="visible-xs">{{ config('app.name', 'City Fresh') }}</span>
                    <button type="button" class="btn btn-navbar navbar-toggle" ><i class="fa fa-bars"></i></button>
                </div>
                <div class="navbar-collapse">
                    <ul class="main-navigation">
                        <li><a href="{{ route('home') }}" class="parent">Home</a> </li>
                        <li><a href="#" class="parent">Shop</a>
                            <ul>
                                @foreach(categories() as $category)
                                     <li>
                                         <a href="{{ route('product.category', ['id' => hashids()->encode($category->id)]) }}">{{ $category->name }}</a>
                                     </li>
                                @endforeach
                            </ul>
                        </li>
                        <li><a href="{{ route('cart') }}" class="parent">Cart</a></li>
                        <li><a href="{{ route('checkout') }}">Checkout</a></li>
{{--                        <li><a href="{{ route('home') }}">About us</a></li>--}}
{{--                        <li><a href="{{ route('home') }}">Contact Us</a> </li>--}}
                        @if(Auth::user() && Auth::user()->user_type_id == 1)
                            <li><a href="{{ route('admin.products') }}">Admin Products</a> </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>
@include('layouts.partials.footer')
</body>
</html>
