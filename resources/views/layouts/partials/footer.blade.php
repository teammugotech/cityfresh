<footer class="cityfresh-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 footer-block">
                <h5 class="footer-title">Information</h5>
                <ul class="list-unstyled ul-wrapper">
                    <li><a href="{{ route('home') }}">About Us</a></li>
                    <li><a href="{{ route('home') }}">Delivery Information</a></li>
                    <li><a href="{{ route('home') }}">Privacy Policy</a></li>
                    <li><a href="{{ route('home') }}">Terms &amp; Conditions</a></li>
                </ul>
            </div>
            <div class="col-sm-3 footer-block">
                <h5 class="footer-title">Customer Service</h5>
                <ul class="list-unstyled ul-wrapper">
                    <li><a href="{{ route('home') }}">Contact Us</a></li>
                    <li><a href="{{ route('home') }}">Returns</a></li>
                    <li><a href="{{ route('home') }}">Site Map</a></li>
                    <li><a href="{{ route('home') }}">Wish List</a></li>
                </ul>
            </div>
            <div class="col-sm-3 footer-block">
                <h5 class="footer-title">Extras</h5>
                <ul class="list-unstyled ul-wrapper">
                    <li><a href="{{ route('home') }}">Brands</a></li>
                    <li><a href="{{ route('home') }}">Specials</a></li>
                </ul>
            </div>
            <div class="col-sm-3 footer-block">
                <div class="content_footercms_right">
                    <div class="footer-contact">
                        <h5 class="contact-title footer-title">Contact Us</h5>
                        <ul class="ul-wrapper">
                            <li><i class="fa fa-map-marker"></i>
                                <span class="location2"> 60 Leucadendron st,<br>
                                Greenville, Fisantekraal<br>
                                Cape Town, South Africa.
                                </span>
                            </li>
                            <li><i class="fa fa-envelope"></i><span class="mail2"><a href="mailto:{{ env('MAIL_TO_EMAIL', 'mugopower@gmail.com') }}">{{ env('MAIL_TO_EMAIL', 'mugopower@gmail.com') }}</a></span></li>
                            <li><i class="fa fa-mobile"></i><span class="phone2"><a href="tel:+27745428879">{{ env('CELL_NUMBER', '0745428879') }}</a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="scrollup">Scroll</a>
</footer>
<div class="footer-bottom">
    <div class="container">
        <div class="copyright">
            <p style="color: #ffffff; font-size: 1.2em;">
                &copy <?php echo date("Y"); ?> <strong>{{ config('app.name', 'City Fresh') }}</strong>. | Made with  <i class="fa fa-heart pulse fa-1x" style="color: #d91616;"></i>  by : &nbsp;<a href="https://facebook.com/mugotech" target="_blank" style="color: #ff753e">MugoTech Web Solutions</a>
            </p>
        </div>
        <div class="footer-bottom-cms">
            <div class="footer-payment">
                <ul>
                    <li class="visa"><a href="{{ url('https://www.visa.co.za/') }}" target="_blank"><img alt="Visa Cards" src="{{ asset('images/payment/visa.jpg') }}" height="30px"></a></li>
                    <li class="payfast"><a href="{{ url('https://www.payfast.co.za/') }}" target="_blank"><img alt="Payfast" src="{{ asset('images/payment/payfast.png') }}" height="30px" style="background-color: #ffffff"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/parally.js') }}" ></script>
<script>
    $('.parallax').parally({offset: -40});
</script>
