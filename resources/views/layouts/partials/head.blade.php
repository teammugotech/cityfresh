<head>
    <title>{{ (isset($title) ? $title . ' | ' : '') . config('app.name', 'City Fresh') }}</title>
    <meta http-equiv="content-type" content="text/html" charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Fruit & Vegetables e-commerce site designed for City Fresh." />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,500,700" rel="stylesheet">

    <!-- Styles -->
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}


    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" media="screen" />
    <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
{{--    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">--}}
    <link href="{{ asset('css/stylesheet.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('owl-carousel/owl.carousel.css') }}" type="text/css" rel="stylesheet" media="screen" />
    <link href="{{ asset('owl-carousel/owl.transitions.css') }}" type="text/css" rel="stylesheet" media="screen" />
    <script src="{{ asset('js/jquery-2.1.1.min.js') }}" ></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" ></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{{ asset('js/jstree.min.js') }}" ></script>
    <script src="{{ asset('js/template.js') }}"  ></script>
    <script src="{{ asset('js/common.js') }}" ></script>
    <script src="{{ asset('js/global.js') }}" ></script>
    <script src="{{ asset('owl-carousel/owl.carousel.min.js') }}" ></script>
    @yield('scripts')
</head>
