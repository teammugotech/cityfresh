<header>
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="top-right pull-right">
                        <div id="top-links" class="nav pull-right">
                            <ul class="list-inline">
                                <li class="dropdown"><a href="#" title="My Account" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user"></i><span>{{ (Auth::user()) ? Auth::user()->name : 'My Account' }}</span> <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        @if(Auth::user())
                                            <li><a href="{{ route('orders') }}" id="wishlist-total" title="Wish List (0)">My Orders</a></li>
                                            <li><a href="{{ route('logout') }}">{{ __('Logout') }}</a></li>
                                        @else
                                            <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                                            <li><a href="{{ route('register') }}">{{ __('Register') }}</a></li>
                                        @endif
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="header-inner">
            <div class="col-sm-4 col-xs-6 header-left" style="margin-top: 20px">
                <div class="shipping">
                    <div class="shipping-img"></div>
                    <div class="shipping-text">
                        <a href="tel:+27745428879">+27 745 428 879</a><br />
                        <span class="shipping-detail">Mon - Fri From 8:00am To 5:00pm</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 header-middle">
                <div class="header-middle-top">
                    <div id="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('images/logo.png') }}" style="width: 250px !important;" title="City Fresh" alt="City Fresh" class="img-responsive" /></a>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 header-right" style="margin-top: 20px">
                <div id="cart" class="btn-group btn-block">
                    <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle cart-dropdown-button">
                        <span id="cart-total">
                            <span class="cart-title">Shopping Cart</span>
                            <br>
                            <span class="cart-quantity">0 item(s) - R {{ (isset($cartSubTotal)) ? number_format($cartSubTotal, 2) :  number_format(0, 2) }}</span>
                        </span>
                    </button>
                    <ul class="dropdown-menu pull-right cart-dropdown-menu">
                        <li>
                            <div>
                                <table class="table table-bordered">
                                    <thead>
                                        <th colspan="2">Cart Summary:</th>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-right"><strong>Items Total</strong></td>
                                        <td class="text-right sub-total-cart">R {{ (isset($cartSubTotal)) ? number_format($cartSubTotal, 2) :  number_format(0, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Delivery Fee</strong></td>
                                        <td class="text-right vat-cart">R {{ (isset($cartDeliveryFee)) ? number_format($cartDeliveryFee, 2) :  number_format(0, 2) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Total</strong></td>
                                        <td class="text-right total-cart">R {{ (isset($cartTotal)) ? number_format($cartTotal, 2) :  number_format(0, 2) }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <p class="text-right">
                                    <span class="btn-viewcart"><a href="{{ route('cart') }}"><strong><i class="fa fa-shopping-cart"></i> View Cart</strong></a></span>
                                    <span class="btn-checkout"><a href="{{ route('checkout') }}"><strong><i class="fa fa-share"></i> Checkout</strong></a></span>
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
