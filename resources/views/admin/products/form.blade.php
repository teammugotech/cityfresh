<div class="row">
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                {!! Form::label('name', 'Product Name') !!}
                {!! Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                @error('name')
                <span class="error-text">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                {!! Form::label('price', 'Product Price') !!}
                {!! Form::text('price', old('price'), ['class' => 'form-control', 'id' => 'price', 'required']) !!}
                @error('price')
                <span class="error-text">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="form-group">
                {!! Form::label('description', 'Product Description') !!}
                {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'rows' => 3, 'id' => 'description', 'required']) !!}
                @error('description')
                <span class="error-text">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                {!! Form::label('category_id', 'Product Category') !!}
                {!! Form::select('category_id', $categories, (isset($product) ? $product->category_id : null), ['class' => 'select2 full-width', 'placeholder' => 'Select category', 'required']) !!}
                @error('category_id')
                <span class="error-text">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="form-group text-center">
                {!! Form::label('in_stock', 'In Stock') !!}
                {!! Form::checkbox('in_stock', 1, (isset($product) && $product->in_stock), ['class' => 'form-control checkbox']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-sm-12">
            <div class="form-group">
                {!! Form::label('image', 'Product Photo') !!}
                <input type="file" name="image" class= "form-control", accept="image/jpeg , image/jpg, image/gif, image/png" required/>
                @error('image')
                <span class="error-text">{{ $message }}</span>
                @enderror
            </div>
        </div>
        @if(isset($product) && file_exists(public_path('uploads/products/'.$product->image)))
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <img class="img-responsive" src="{{ asset('uploads/products/'.$product->image)  }}" />
                </div>
            </div>
        @endif
    </div>

</div>
