@php
    $title = "Edit product";
@endphp
@extends('layouts.app')

@section('content')

    <div class="container">
        <ul class="breadcrumb">
            <li><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i></a></li>
            <li><a href="{{ route('admin.products') }}">Admin Products</a></li>
            <li><a href="{{ route('admin.product.edit', hashids()->encode($product->id)) }}">Edit Product</a></li>
        </ul>
        <div class="row">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-error" role="alert">
                    {{ session('error') }}
                </div>
            @endif
            <div class="col-sm-12 mb-5" id="content">
                <div class="col-sm-12 col-md-12 m-b-20">
                    <a class="btn btn-primary pull-left" href="{{ route('admin.products') }}"><i class="fa fa-chevron-left m-r-10"></i>Back to products</a>
                </div>
                <h1>Edit Product</h1>
                {!! Form::model($product, ['id' => 'edit-product', 'route' => ['admin.product.update', hashids()->encode($product->id)], 'method' => 'POST', 'files' => true]) !!}
                    @include('admin.products.form')
                    <div class="row m-t-30">
                        {!! Form::submit('Update product', ['class' => 'btn btn-primary pull-right']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
