@php
    $title = "Products";
@endphp
@extends('layouts.app')

@section('content')

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-error" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <ul class="breadcrumb">
            <li><a href="{{ route('admin.users') }}"><i class="fa fa-users"></i></a></li>
            <li><a href="{{ route('admin.products') }}">Admin Products</a></li>
        </ul>
        <div class="row">
            <div class="col-sm-12 mb-5" id="content">
                <div class="col-sm-12 col-md-12 m-b-20">
                    <a class="btn btn-primary pull-right" href="{{ route('admin.product.create') }}"><i class="fa fa-plus m-r-10"></i>Create product</a>
                    <a class="btn btn-primary pull-right m-r-20" href="{{ route('admin.categories') }}"><i class="fa fa-bars m-r-10"></i>Product Categories</a>
                    <a class="btn btn-primary pull-right m-r-20" href="{{ route('admin.products', ($active) ? 0 : 1) }}"><i class="fa {{ ($active) ? 'fa-ban' : 'fa-plus' }} m-r-10"></i>{{ ($active) ? 'In-active products' : 'Active products' }}</a>
                </div>
                <h1>{{  ($active) ? 'Admin Active products' : 'Admin In-active products' }}</h1>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-left">Product Name</th>
                                <th class="text-center">Category</th>
                                <th class="text-right">Price</th>
                                <th class="text-center">Active</th>
                                <th class="text-center">In Stock</th>
                                <th class="text-right">Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr class="product-row">
                                    <td class="text-left" style="width: 75px;">{{ $product->name }}</td>
                                    <td class="text-center">{{  $product->category->name }}</td>
                                    <td class="text-right">{{  $product->price }}</td>
                                    <td class="text-center">{{  $product->trashed() ? "Not active" : "Active" }}</td>
                                    <td class="text-center">{{  $product->in_stock ? "In stock" : "Out of stock" }}</td>
                                    <td class="text-right">
                                        <span class="">
                                            <a href="{{ route('admin.product.edit', hashids()->encode($product->id)) }}" data-toggle="tooltip" title="Edit product" data-id="{{ hashids()->encode($product->id) }}">
                                                <i class="fa fa-pencil m-r-10"></i>
                                            </a>
                                        </span>
                                        <span class="">
                                            <a href="{{ route('admin.product.activate', hashids()->encode($product->id)) }}" data-toggle="tooltip" title="{{ $product->trashed() ? 'Activate product' : 'De-activate product' }}">
                                                <i class="fa {{ $product->trashed() ? 'fa-refresh' : 'fa-ban' }} m-r-10"></i>
                                            </a>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="mb-5">

                </div>
            </div>
        </div>
    </div>
@endsection
