@php
    $title = "Checkout";
@endphp
@extends('layouts.app')

@section('content')

    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-error" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <div class="row">
            <ul class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                <li><a href="{{ route('checkout') }}">Checkout</a></li>
            </ul>
        </div>
        <div class="row">
            @if(isset($cart))
                <div class="col-sm-12 mb-5" id="content">
                    <form class="form-horizontal" method="POST" action="{{ route('order.save') }}">
                        @csrf
                        <div class="row delivery-address">
                            <h1>Confirm your delivery Address.</h1>
                            <div class="radio">
                                <label>
                                    <input type="radio" checked="checked" value="existing" name="shipping_address">
                                    I want to use an existing address
                                </label>
                            </div>
                            <div id="shipping-existing" >
                                <select class="form-control" name="address_id">
                                    <option selected="selected" value="">Choose from my saved Address</option>
                                    @foreach($addresses as $address)
                                        <option  value="{{ hashids()->encode(1) }}">{!! $address->address_1 . ', ' . $address->address_2  . ', ' . $address->city !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" value="new" name="shipping_address">
                                    I want to use a new address
                                </label>
                            </div>
                            <br>

                            <div id="shipping-new" style="display: none;">
                                <div class="form-group">
                                    <label for="contact_person" class="col-sm-2 control-label">Contact Person</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="contact_person" placeholder="Contact Person" value="" name="contact_person">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="contact_number" class="col-sm-2 control-label">Contact Number</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="contact_number" placeholder="Contact Number" value="" name="contact_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="company" class="col-sm-2 control-label">Company Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="company" placeholder="Company" value="" name="company">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address_1" class="col-sm-2 control-label">Address 1</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="address_1" placeholder="Address Line 1" value="" name="address_1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address_2" class="col-sm-2 control-label">Address 2</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="address_2" placeholder="Address Line 2" value="" name="address_2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="city" class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="city" placeholder="City" value="" name="city">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="postcode" class="col-sm-2 control-label">Post Code</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="postcode" placeholder="7550" value="" name="postcode">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="state" class="col-sm-2 control-label">Province / State</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="state" placeholder="Province" value="" name="state">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country" class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="country" name="country_id">
                                            <option value="1" selected="selected">South Africa</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row order-summary">
                            <h1>Confirm your order.</h1>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">

                                    <thead>
                                        <tr>
                                            <td class="text-left">Product Image</td>
                                            <td class="text-left">Product Name</td>
                                            <td class="text-right">Quantity</td>
                                            <td class="text-right">Unit Price</td>
                                            <td class="text-right">Total</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($cart->items as $item)
                                            <tr>
                                                <td class="text-center" style="width: 75px;">
                                                    <a href="{{ route('product.view', hashids()->encode($item->product->id)) }}">
                                                        <img class="img-thumbnail" title="{{ $item->product->name }}" alt="{{ $item->product->name }}" src="{{ asset('uploads/products/'.$item->product->image) }}">
                                                    </a>
                                                </td>
                                                <td class="text-left"><a href="{{ route('product.view', hashids()->encode($item->product->id)) }}">{{ $item->product->name }}</a></td>
                                                <td class="text-right">{{ $item->quantity }}</td>
                                                <td class="text-right">R {{ number_format($item->price, 2) }}</td>
                                                <td class="text-right">R {{ number_format($item->total, 2) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <td class="text-right" colspan="3"><strong>Sub-Total:</strong></td>
                                            <td class="text-right">R {{ number_format($cart->items_total, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="3"><strong>Flat Shipping Rate:</strong></td>
                                            <td class="text-right">R {{ number_format($cart->delivery_fee, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="3"><strong>Total:</strong></td>
                                            <td class="text-right">R {{ number_format($cart->total, 2) }}</td>
                                        </tr>
                                    </tfoot>

                                </table>
                            </div>
                        </div>

                        <div class="row payment method">
                            <h1>Please select the preferred payment method to use on this order.</h1>
                            @foreach($paymentMethods as $paymentMethod)
                                <div class="radio">
                                    <label>
                                        <input type="radio" {{ ($paymentMethod->id == 1) ? 'checked' : '' }} value="{{ hashids()->encode($paymentMethod->id) }}" name="payment_method">
                                        {{ $paymentMethod->name }}
                                    </label>
                                </div>
                            @endforeach

                            <p>
                                <strong>Add Comments About Your Order</strong>
                            </p>
                            <p>
                                <textarea class="form-control" rows="4" name="comment"></textarea>
                            </p>

                            <div class="buttons mt-5 mb-5">
                                <div class="pull-right">
                                    <label for="terms-and-conditions">I have read and agree to the </label><a class="agree" href="#"><b> Terms &amp; Conditions</b></a>
                                    <input id="terms-and-conditions" type="checkbox" value="1" name="agree">
                                    <br />
                                    <button type="submit" data-loading-text="Loading..." class="btn btn-primary pull-right mt-5" id="button-confirm" value="Place Order">Place Order</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            @else
                <div class="alert alert-info">
                    You dont have any items to checkout yet, please add some products first in your shopping cart.
                </div>
            @endif
        </div>
    </div>
@endsection
