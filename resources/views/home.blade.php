@php
    $title = "Home";
@endphp
@extends('layouts.app')

@section('content')

<div class="mainbanner">
    <div id="main-banner" class="owl-carousel home-slider">
        <div class="item"> <a href="#"><img src="images/banners/Main-Banner1.jpg" alt="main-banner1" class="img-responsive" /></a> </div>
        <div class="item"> <a href="#"><img src="images/banners/Main-Banner2.jpg" alt="main-banner2" class="img-responsive" /></a> </div>
        <div class="item"> <a href="#"><img src="images/banners/Main-Banner3.jpg" alt="main-banner3" class="img-responsive" /></a> </div>
    </div>
</div>
<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-error" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
        <div id="content" class="col-sm-12">
            <div class="blog">
                <div class="blog-heading">
                    <h3>Product Categories</h3>
                </div>
                <div class="blog-inner box">
                    <ul class="list-unstyled blog-wrapper" id="latest-blog">
                        @foreach($categories as $category)
                            <li class="item blog-slider-item">
                                <div class="panel-default">
                                    <div class="blog-image">
                                        <a href="#" class="blog-imagelink">
                                            <img src="images/blog/blog_1.jpg" alt="#">
                                        </a>
                                        <span class="blog-hover"></span> <span class="blog-date">{{ $category->name }}</span>
                                        <span class="blog-readmore-outer">
                                            <a href="{{ route('product.category', ['id' => hashids()->encode($category->id)]) }}" class="blog-readmore">Shop {{ $category->name }}</a>
                                        </span>
                                    </div>
                                    <div class="blog-content">
                                        <a href="{{ route('product.category', ['id' => hashids()->encode($category->id)]) }}" class="blog-name">
                                            <h2>{{ $category->name }}</h2>
                                        </a>
                                        <div class="blog-desc">
                                            {!! $category->description !!}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <h3 class="productblock-title">Best Selling Products</h3>
            <div class="">
                <div class="col-md-6 list-grid-wrapper">
                    <div class="btn-group btn-list-grid">
                        <button type="button" id="grid-view" class="btn btn-default grid" data-toggle="tooltip" title="Grid"><i class="fa fa-th hide"></i></button>
                    </div>
                </div>
            </div>
            <br />
            <div class="grid-list-wrapper">
                @foreach($products as $product)
                    <div class="product-layout product-list col-xs-12">
                        <div class="product-thumb">
                            <div class="image product-imageblock">
                                <a href="{{ route('product.view', hashids()->encode($product->id)) }}"> <img src="{{ asset('uploads/products/' . $product->image) }}" alt="{{ $product->name }}" title="{!! $product->description !!}" class="img-responsive" /> </a>
                                <div class="button-group">
                                    <button type="button" class="addtocart-btn add-to-cart instant" data-quantity="1" data-id="{{ hashids()->encode($product->id) }}">Add to Cart</button>
                                </div>
                            </div>
                            <div class="caption product-detail">
                                <h4 class="product-name">
                                    <a href="{{ route('product.view', hashids()->encode($product->id)) }}" title="{!! $product->description !!}">{{ $product->name }}</a>
                                </h4>
                                <p class="product-desc">{!! $product->description !!}</p>
                                <p class="price product-price">{{ 'R ' . number_format($product->price, 2) }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="blog-heading"><h3>Brands that trust us!</h3></div>
            <div id="brand_carouse" class="owl-carousel brand-logo">
                <div class="item text-center"> <a href="#"><img src="images/brand/brand1.png" alt="Disney" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand2.png" alt="Dell" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand3.png" alt="Harley" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand4.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand6.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand7.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand8.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand9.png" alt="Canon" class="img-responsive" /></a> </div>
                <div class="item text-center"> <a href="#"><img src="images/brand/brand5.png" alt="Canon" class="img-responsive" /></a> </div>
            </div>
        </div>
    </div>
</div>
@endsection
