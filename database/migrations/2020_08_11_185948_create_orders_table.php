<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->integer('payment_method_id')->unsigned();
            $table->integer('delivery_address_id')->unsigned();
            $table->decimal('items_total', 8, 2)->default(0.00);;
            $table->decimal('delivery_fee', 8, 2)->default(0.00);;
            $table->decimal('vat', 8, 2)->default(0.00);
            $table->decimal('total', 8, 2)->default(0.00);
            $table->longText('note')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
