<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTypesSeeder::class);
        $this->call(ProductCategorySeeder::class);
        $this->call(PaymentMethodsSeeder::class);
        $this->call(PaymentStatusSeeder::class);
    }
}
