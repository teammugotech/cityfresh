<?php

use App\PaymentStatus;
use Illuminate\Database\Seeder;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = array(
            [
                'id' => 1,
                'status' => 'Pending'
            ],
            [
                'id' => 2,
                'status' => 'Complete'
            ],
            [
                'id' => 3,
                'status' => 'Failed'
            ],
            [
                'id' => 4,
                'status' => 'Other'
            ]
        );

        foreach ($statuses as $status){
            PaymentStatus::updateOrCreate(
                [
                    'id' => $status['id']
                ],
                [
                    'status' => $status['status']
                ]
            );
        }
    }
}
