<?php

use App\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paymentMethods = array(
            [
                'id' => 1,
                'name' => 'PayFast'
            ],
            [
                'id' => 2,
                'name' => 'Cash On Delivery'
            ]
        );

        foreach ($paymentMethods as $paymentMethod){
            PaymentMethod::updateOrCreate(
                [
                    'id' => $paymentMethod['id']
                ],
                [
                    'name' => $paymentMethod['name']
                ]
            );
        }
    }
}
