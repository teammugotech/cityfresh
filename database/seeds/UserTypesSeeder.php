<?php

use App\UserType;
use Illuminate\Database\Seeder;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userTypes = array(
            [
                'id' => 1,
                'name' => 'Admin'
            ],
            [
                'id' => 2,
                'name' => 'Customer'
            ],
            [
                'id' => 3,
                'name' => 'Delivery'
            ]
        );

        foreach ($userTypes as $userType){
            UserType::updateOrCreate(
                [
                    'id' => $userType['id']
                ],
                [
                    'name' => $userType['name']
                ]
            );
        }
    }
}
