<?php

use App\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array(
            [
                'id' => 1,
                'name' => 'Vegetables',
                'description' => 'A variety of fresh farm vegetable produce.'
            ],
            [
                'id' => 2,
                'name' => 'Fruits',
                'description' => 'A variety of fresh farm fruits.'
            ],
            [
                'id' => 3,
                'name' => 'Herbs',
                'description' => 'A wide range of herbs.'
            ],
            [
                'id' => 4,
                'name' => 'Beverages',
                'description' => 'Beverages category including wines and alcohol.'
            ]
        );

        foreach ($categories as $category){
            ProductCategory::updateOrCreate(
                [
                    'id' => $category['id']
                ],
                [
                    'name' => $category['name'],
                    'description' => $category['description']
                ]
            );
        }
    }
}
