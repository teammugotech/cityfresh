$(document).ready(function() {

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });

    $('.select2').select2();

	$('#main-banner,.gellery').owlCarousel({
		autoPlay: 5000,
		singleItem: true,
		navigation: false,
		navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
		pagination: true,
		transitionStyle : "fade"
	});

	$('#content #special-slider').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});
	$('#content #feature-slider').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});

	$('#latest-slidertab').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});
	$('#special-slidertab').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});
	$('#related-slidertab').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});

	$('#Weekly-slider').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});

	$('#bestseller-slidertab').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});

	 $('#brand_carouse').owlCarousel({
        items: 5,
        navigation: true,
        pagination: false
    });

	$('#testimonial').owlCarousel({
		items: 1,
		autoPlay: true,
		navigation: false,
		pagination: true,
		transitionStyle: 'fadeUp',
		itemsDesktop : [1199, 1],
		itemsDesktopSmall : [979, 1],
		itemsTablet : [768, 1]
	});
	$('#content #latest-blog').owlCarousel({
		items: 3,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 2],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});
	$('#related-slider').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 3],
		itemsDesktopSmall : [979, 2],
		itemsTablet : [768, 2],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});
	$('#product-thumbnail').owlCarousel({
		items: 4,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 4],
		itemsDesktopSmall : [979, 3],
		itemsTablet : [768, 4],
		itemsTabletSmall : false,
		itemsMobile : [479, 3]
	});
	$('#testimonial').owlCarousel({
		items: 1,
		navigation: true,
		pagination: false,
		itemsDesktop : [1199, 1],
		itemsDesktopSmall : [979, 1],
		itemsTablet : [768, 1],
		itemsTabletSmall : false,
		itemsMobile : [479, 1]
	});

    $('input[name="shipping_address"]').off('change');
    $('input[name="shipping_address"]').on('change', function() {
        if ($(this).val() == 'new') {
            $('#shipping-existing').hide();
            $('#shipping-new').show();
        } else {
            $('#shipping-existing').show();
            $('#shipping-new').hide();
        }
    });

    $(document).off('click', '.add-to-cart');
    $(document).on('click', '.add-to-cart', function (e){
        e.preventDefault();
        var button = $(this);
        var id = $(this).attr('data-id');
        if($(this).hasClass('instant')){
            var quantity = $(this).attr('data-quantity');
            var instant = 1;
        } else{
            var quantity = $('.productpage-qty').val();
            var instant = 0;
        }

        $.ajax({
            url: '/cart/add',
            type: "POST",
            async: true,
            data: {
                id: id,
                instant: instant,
                quantity: quantity
            },
            beforeSend: function() {
                button.html('Adding...');
            },
            success: function(response) {
                $('.cart-quantity').html(response.counter + ' item(s) - R ' + response.total);
                button.html('Added to cart');
                Toast.fire({
                    icon: 'success',
                    title: 'Product added to cart!'
                });
            },
            error: function (response){
                console.log(response);
                Toast.fire({
                    icon: 'error',
                    title: 'Something went wrong!'
                });
            }
        })
    });

    $(document).off('change', '.update-quantity');
    $(document).on('change', '.update-quantity', function (e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var quantity = $(this).val();
        var instant = 0;


        $.ajax({
            url: '/cart/add',
            type: "POST",
            async: true,
            data: {
                id: id,
                instant: instant,
                quantity: quantity
            },
            success: function(response) {
                Toast.fire({
                    icon: 'success',
                    title: 'Item updated successfully!'
                });
                location.reload();
            },
            error: function (response){
                Toast.fire({
                    icon: 'error',
                    title: 'Something went wrong!'
                });
            }
        })
    });

    $(document).off('click', '.cart-remove');
    $(document).on('click', '.cart-remove', function (e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        console.log('swal');

        Swal.fire({
            title: title,
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/cart/delete',
                    type: 'GET',
                    data: {
                        id: id
                    },
                    success: function(data)
                    {
                        if (data.success){
                            $(".product-row[data-id='"+id+"']").slideUp();
                            Swal.fire({
                                title: data.message,
                                text: "",
                                icon: "success",
                                timer: 1500,
                                showConfirmButton: false
                            })
                        }else{
                            Swal.fire(
                                data.message,
                                '',
                                'error'
                            )
                        }
                    },
                    error: function() {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!'
                        });
                    }
                });
            }
        });
    });

    /**
     * END OF .READY FUNCTION
     */

});
$(window).load(function() {
$(".preloader").removeClass("loader");
$(".preloader").css("display","none");
});

$.fn.tabs = function() {
	var selector = this;
	this.each(function() {
		var obj = $(this);
		$(obj.attr('href')).hide();
		obj.click(function() {
			$(selector).removeClass('selected');
			$(this).addClass('selected');
			$($(this).attr('href')).fadeIn();
			$(selector).not(this).each(function(i, element) {
					$($(element).attr('href')).hide();
			});
			return false;
		});
	});
	$(this).show();
	$(this).first().click();
};






