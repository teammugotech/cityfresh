# CITY FRESH PROJECT
CITY FRESH - Online Fruit & Vegetables Shop with CMS (Content Management System).

## Table of Contents

- [Features](README.md#markdown-header-features)
- [System Requirements](README.md#markdown-header-system-requirements)
- [Software Used](README.md#markdown-header-software-used)
- [Links](README.md#links)
- [Installation](README.md#markdown-header-installation)
- [Security Vulnerabilities and Feedback](README.md#markdown-header-security-vulnerabilities-and-feedback)
- [License](README.md#markdown-header-license)

## Features
- Online Fruit & Vegetables Shop.
- Content Management System.

## System Requirements
- PHP > 7.2
- PHP Extensions: PDO, cURL, Bcmath, Mcrypt, XML, GD
- Node.js > 6.0
- Composer > 1.0.0
- Laravel > 6.x
- MySql > 14.0
- Apache > 2.4

## Software Used
- [Laravel](https://laravel.com)
- [Bootstrap](https://getbootstrap.com/)
- [NodeJs](https://nodejs.org/)
- [jQuery](https://jquery.com/)
- CSS

## Links

**[Staging Site](https://cityfresh.digitaltechno.co.za/)**

**[Trello Board](https://trello.com/b/wN2ddd2V/city-fresh)**

**[Developer FB Page](https://facebook.com/mugotech)**


## Installation

Clone repository

`$ git clone git@bitbucket.org:teammugotech/cityfresh.git`

Change into the working directory

`$ cd cityfresh`

Copy `.env.example` to `.env` and modify according to your environment

`$ cp .env.example .env`

Install composer dependencies

`$ composer install --prefer-dist`

Install node dependencies

`$ npm install && npm run dev`

Change folder permissions

`$ sudo chown -R www-data:www-data storage && sudo chown -R www-data:www-data vendor && sudo chown -R www-data:www-data public && sudo chown -R www-data:www-data node_modules && sudo chown -R www-data:www-data bootstrap/cache && sudo chmod -R 775 storage bootstrap/cache && sudo chmod -R 775 storage public`

An application key can be generated with the command

`$ php artisan key:generate`

Run these commands to create the tables within the defined database and populate seed data

`$ php artisan migrate --seed`
## Security Vulnerabilities and Feedback
If you discover a security vulnerability within [City Fresh](https://bitbucket.org/teammugotech/cityfresh/), please send an e-mail to:
- Mugo Machaka via [juniormachaka@gmail.com](mailto:juniormachaka@gmail.com)
- Tsana Machaka via [tsananguraim@gmail.com](mailto:tsananguraim@gmail.com).

All security vulnerabilities will be promptly addressed.
## License

The [City Fresh](https://bitbucket.org/teammugotech/cityfresh/) is an open-source software built on [Laravel Framework](https://github.com/laravel). Any developers are free to use for personal use and or contribute if you have passion in providing support to E-Commerce Shops. City Fresh is licensed under the [MIT license](https://opensource.org/licenses/MIT).
