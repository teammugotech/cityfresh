<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'orders';
    protected $dates = ['deleted_at'];

    /**
     * @return HasMany
     */
    public function items(){
        return $this->hasMany(OrderItem::class, 'order_id');
    }

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function address(){
        return $this->belongsTo(DeliveryAddress::class, 'delivery_address_id');
    }
}
