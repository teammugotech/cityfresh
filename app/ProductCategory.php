<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'product_categories';
    protected $dates = ['deleted_at'];
}
