<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return HasMany
     */
    public function addresses(){
        return $this->hasMany(DeliveryAddress::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function orders(){
        return $this->hasMany(Order::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function payments(){
        return $this->hasMany(Payment::class, 'user_id');
    }

    /**
     * @return HasOne
     */
    public function cart(){
        return $this->hasOne(Cart::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function userType(){
        return $this->belongsTo(UserType::class, 'user_type_id');
    }
}
