<?php

use App\ProductCategory;
use Hashids\Hashids;
use Illuminate\Database\Eloquent\Collection;

/**
 * @return ProductCategory[]|Collection
 */
function categories(){
    return ProductCategory::all();
}

/**
 * @return Hashids
 */
function hashids(){
    return  new Hashids();
}

/**
 * @param $image
 * @param $public_path
 * @return string|string[]
 */
function uploadImage($image, $public_path) {
    // Set the ful public path
    $target_dir = public_path($public_path);
    // See if folder exists
    if(!File::exists($target_dir)) {
        // Create folder
        File::makeDirectory($target_dir, $mode = 0775, true, true);
    }
    $image_url = '';
    // Get timestamp
    $date = new \DateTime();
    $date = $date->format('YmdHis');
    // Create file name
    $target_file = $target_dir . '/' . $date . '_' . $image . '_' . basename($_FILES[$image]['name']);
    // Okay to upload image
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // check for fake image
    if (!getimagesize($_FILES[$image]["tmp_name"])) {
        $uploadOk = 0;
    }

    // Check if all the checks have passed and upload the image
    $target_file = str_replace(' ', '_', $target_file); //Replaces spaces in uploads filename
    if ($uploadOk === 1 && move_uploaded_file($_FILES[$image]["tmp_name"], $target_file)) {
        $image_url = str_replace(' ', '_', $date . '_' . $image . '_' . basename($_FILES[$image]['name']));
        return $image_url;
    } else {
        $image_url = false; /* used to redirect when error occurs */
        return $image_url;
    }
}
