<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Hashids\Hashids;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ProductCategoryController extends Controller
{
    protected $hashids;
    protected $product;
    protected $category;

    /**
     * Create a new controller instance.
     *
     * @param Hashids $hashids
     * @param Product $product
     * @param ProductCategory $category
     */
    public function __construct(Hashids $hashids, Product $product, ProductCategory  $category){
        $this->hashids = $hashids;
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function products($id){
        $catId = $this->hashids->decode($id);
        $category = $this->category->where('id', $catId)->first();
        $products = $this->product->where('category_id', $category->id)->get();

        return view('products.category', compact('products', 'category'));
    }
}
