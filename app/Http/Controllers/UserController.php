<?php

namespace App\Http\Controllers;

use App\DeliveryAddress;
use App\User;
use Hashids\Hashids;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct
    (
        DeliveryAddress $address,
        Hashids $hashids,
        User $user
    )
    {
        $this->middleware('auth');
        $this->address = $address;
        $this->hashids = $hashids;
        $this->user = $user;
    }

    public function index($active = 1){
        $users = $this->user->get();
        dd($users);
    }
}
