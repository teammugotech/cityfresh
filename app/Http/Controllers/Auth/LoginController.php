<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\CartController;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {

        $cartSession = app(CartController::class)->getCartSession();
        if($cartSession){
            app(CartController::class)->saveSessionItemsToCart();
        }
    }

    /**
     * @param Request $request
     * @return Application|RedirectResponse|Redirector|mixed
     */
    public function logout(Request $request)
    {
        $cartItems =  app(CartController::class)->getCartSession();
        $this->guard()->logout();

        $request->session()->invalidate();
        $request->session()->regenerate();
        session()->put('cartItems', $cartItems);
        session()->save();
        return $this->loggedOut($request) ?: redirect('/');
    }
}
