<?php

namespace App\Http\Controllers;

use App\Cart;
use App\DeliveryAddress;
use App\Order;
use App\OrderItem;
use App\Product;
use Hashids\Hashids;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class OrderController extends Controller
{
    protected $address;
    protected $hashids;
    protected $product;
    protected $order;
    protected $item;

    /**
     * CartController constructor.
     * @param DeliveryAddress $address
     * @param Hashids $hashids
     * @param Product $product
     * @param Order $order
     * @param OrderItem $item
     */
    public function __construct
    (
        DeliveryAddress $address,
        Hashids $hashids,
        Product $product,
        Order $order,
        OrderItem $item
    )
    {
        $this->middleware('auth');
        $this->address = $address;
        $this->hashids = $hashids;
        $this->product = $product;
        $this->order = $order;
        $this->item = $item;
    }

    /**
     * View User Order
     * @param $id
     * @return Application|Factory|View
     */
    public function view($id){
        $id = $this->hashids->decode($id)[0];
        $order = $this->order
            ->with('items')
            ->where('id', $id)
            ->first();

        return view('order', compact('order'));
    }

    /**
     * User Orders
     */
    public function orders(){
        $orders = $this->order->with('items')->where('user_id', Auth::id())->get();
        return view('orders', compact('orders'));
    }

    /**
     * Create | Save an order
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request){
        // save new address
        if($request->shipping_address == 'new'){
            $address = $this->addDeliveryAddress($request);
            $addressId = $address->id;
        } else{
            $addressId = $this->hashids->decode($request->address_id)[0];
        }
        $paymentMethodId = $this->hashids->decode($request->payment_method)[0];

        //create order
        $order = $this->order;
        $order->user_id = Auth::id();
        $order->payment_method_id = $paymentMethodId;
        $order->delivery_address_id = $addressId;
        $order->delivery_fee = $this->getDeliveryFee();
        $order->note = $request->comment;
        $order->vat = 0;
        $order->save();

        $cart = Cart::where('user_id', Auth::id())->first();
        $itemsTotal = 0;
        foreach ($cart->items as $cartItem){
            $item = $this->item;
            $item->order_id = $order->id;
            $item->product_id = $cartItem->product_id;
            $item->quantity = $cartItem->quantity;
            $item->price = $cartItem->price;
            $item->total = ($cartItem->price * $cartItem->quantity);
            $item->save();
            $itemsTotal += $item->total;
        }

        // Update order model
        $order->items_total = $itemsTotal;
        $order->total = ($itemsTotal + $order->delivery_fee + $order->vat);
        $order->save();

        //Clear cart
        $cart->items()->delete();
        $cart->delete();

        return redirect()->route('orders')->with('message', 'Thank you! Your order was placed successfully.');

    }

    /**
     * Add Delivery Address
     * @param Request $request
     * @return DeliveryAddress
     */
    public function addDeliveryAddress(Request $request){
        $address = $this->address;
        $address->contact_person = $request->contact_person;
        $address->contact_number = $request->contact_number;
        $address->country_id = $request->country_id;
        $address->address_1 = $request->address_1;
        $address->address_2 = $request->address_2;
        $address->postcode = $request->postcode;
        $address->company = $request->company;
        $address->state = $request->state;
        $address->city = $request->city;
        $address->user_id = Auth::id();
        $address->save();

        return $address;
    }

    /**
     *  Get Delivery Fee
     * @return float
     */
    public function getDeliveryFee(){
        return 50.00;
    }
}
