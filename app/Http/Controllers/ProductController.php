<?php

namespace App\Http\Controllers;

use App\CartItem;
use App\Product;
use App\ProductCategory;
use Hashids\Hashids;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Validator;

class ProductController extends Controller
{
    protected $hashids;
    protected $product;
    protected $category;
    protected $item;

    /**
     * ProductController constructor.
     * @param Hashids $hashids
     * @param Product $product
     * @param CartItem $item
     */
    public function __construct(Hashids $hashids, Product $product, CartItem $item, ProductCategory $category){
        $this->hashids = $hashids;
        $this->product = $product;
        $this->item = $item;
        $this->category = $category;
    }

    /**
     * @param null $id
     * @return Application|Factory|RedirectResponse|View
     */
    public function view($id = null){
       if ($id){
           $prodId = $this->hashids->decode($id)[0];
           $product = $this->product->where('id', $prodId)->first();
           $item = $this->item->where('product_id', $product->id)->first();
           $relatedProducts = $this->product->where('category_id', $product->category_id)
               ->whereNotIn('id', [$product->id])
               ->limit(10)
               ->get();
       }else{
           return redirect()->back()->with('status', 'No product has been specified!');
       }

       return view('products.view', compact('item', 'product', 'relatedProducts'));
   }

    /**
     * Admin product list
     * @param int $active
     * @return Application|Factory|View
     */
   public function products($active = 1){
        $products = ($active) ? $this->product->get() : $this->product->onlyTrashed()->get();
        return view('admin.products.index', compact('products', 'active'));
   }

    /**
     * Admin Create Product
     * @return Application|Factory|View
     */
   public function create(){
       $categories = $this->category->pluck('name', 'id')->toArray();
       return view('admin.products.create', compact('categories'));
   }

    /**
     * Admin Save Product
     * @param Request $request
     * @return RedirectResponse
     */
   public function save(Request $request){
       $validator = Validator::make($request->all(), [
           "name" => "required",
           'image' => "required|mimes:jpeg,jpg,png,gif",
           'price' => "required",
           'description' => "required",
           'category_id' => "required"
       ]);
       if ($validator->fails()) {
           return redirect()->back()->withErrors($validator)->withInput();
       }

       $image = uploadImage('image', 'uploads/products');
       if (!$image){
           return redirect()->back()->with('error', 'Image upload failed, make sure the size is not too big!')->withInput();
       }

       $product = $this->product;
       $product->name = $request->name;
       $product->price = $request->price;
       $product->description = $request->description;
       $product->category_id = $request->category_id;
       $product->in_stock = $request->has('in_stock');
       $product->image = $image;
       $product->save();

       return redirect()->back()->with('status', 'Product added successfully!');
   }

    /**
     * Admin Edit Product
     * @param $id
     * @return Application|Factory|View
     */
   public function edit($id){
       $id = $this->hashids->decode($id)[0];
       $product = $this->product->where('id', $id)->first();
       $categories = $this->category->pluck('name', 'id')->toArray();

       return view('admin.products.edit', compact('product', 'categories'));
   }

    /**
     * Admin Update product
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
   public function update(Request $request, $id){
       $validator = Validator::make($request->all(), [
           "name" => "required",
           'price' => "required",
           'description' => "required",
           'category_id' => "required"
       ]);

       if ($validator->fails()) {
           return redirect()->back()->withErrors($validator)->withInput();
       }

       $id = $this->hashids->decode($id)[0];
       $product = $this->product->where('id', $id)->first();
       if($request->hasFile('image')){
           $image = uploadImage('image', 'uploads/products');
           if (!$image){
               return redirect()->back()->with('error', 'Image upload failed, make sure the size is not too big!')->withInput();
           }elseif(file_exists(public_path('uploads/products/'.$product->image))){
               //Delete old image file from the server
               unlink(public_path('uploads/products/'.$product->image));
           }
       }

       $product->name = $request->name;
       $product->price = $request->price;
       $product->description = $request->description;
       $product->category_id = $request->category_id;
       $product->in_stock = $request->has('in_stock');
       $product->image = isset($image) ? $image : $product->image;
       $product->save();

       return redirect()->back()->with('status', 'Product updated successfully!');
   }

   public function activate($id){
       $id = $this->hashids->decode($id)[0];
       $product = $this->product->withTrashed()->where('id', $id)->first();
       if($product->trashed()){
           $product->restore();
           $message = 'restored';
       } else{
           $product->delete();
           $message = 'deleted';
       }

       return redirect()->back()->with('status', 'Product ' . $message . ' successfully!');
   }
}
