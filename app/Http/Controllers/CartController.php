<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartItem;
use App\DeliveryAddress;
use App\PaymentMethod;
use App\Product;
use Hashids\Hashids;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class CartController extends Controller
{
    protected $hashids;
    protected $product;
    protected $cart;
    protected $item;

    /**
     * CartController constructor.
     * @param Hashids $hashids
     * @param Product $product
     * @param Cart $cart
     * @param CartItem $item
     */
    public function __construct(Hashids $hashids, Product $product, Cart $cart, CartItem $item){
        $this->hashids = $hashids;
        $this->product = $product;
        $this->cart = $cart;
        $this->item = $item;
    }

    /**
     * User Cart
     * @return Application|Factory|View
     */
    public function cart(){
        $cart = null;
        if(Auth::user()){
            $cart = $this->cart->where('user_id', Auth::id())->first();
        } else{
            if($this->getCartSession()){
                $cart = new CartItem();
                $cart->user_id = 0;
                $cart->items = $this->formatCartItems();
                $cart->delivery_fee = $this->getDeliveryFee();
                $cart->vat = 0;
                $cart->items_total = $this->cartItemsTotal();
                $cart->total = ($cart->items_total + $cart->delivery_fee + $cart->vat);
            }
        }

        return view('cart', compact('cart'));
    }

    /**
     * Add to cart method
     * @param Request $request
     * @return JsonResponse
     */
    public function add(Request $request){
        $id = $this->hashids->decode($request->id)[0];
        $product = $this->product->where('id', $id)->first();
        if(Auth::user()){
            $cart = $this->cart->where('user_id', Auth::id())->first();
            if($cart == null){
                $cart = new $this->cart;
                $cart->user_id = Auth::id();
                $cart->delivery_fee = $this->getDeliveryFee();
                $cart->save();
            }

            $item = $this->item->where(['cart_id' => $cart->id, 'product_id' => $product->id])->first();
            if ($item == null){
                $item = new $this->item;
                $item->cart_id = $cart->id;
                $item->product_id = $product->id;
                $item->quantity = $request->quantity;
            } else{
                // Update quantity by adding or overwriting
                $item->quantity = ($request->instant) ? ($item->quantity + $request->quantity) :  $request->quantity;
            }
            $item->price = $product->price;
            $item->total = ($item->price * $item->quantity);
            $item->save();
            $this->updateCart();

        } else{
            $cartItems = $this->getCartSession();
            $update = false;

            if($cartItems != null){
                foreach ($cartItems as &$item){
                    if(($item['product_id'] == $product->id)){
                        $item['price'] = $product->price;
                        $item['quantity'] = ($request->instant) ? ($item['quantity'] + $request->quantity) : $request->quantity; // Update quantity by adding or overwriting
                        $item['total'] = ($item['price'] * $item['quantity']);
                        $update = true;
                        break;
                    }
                }
                if(!$update){
                    $cartItems[] = $this->cartItemArray($request->quantity, $product);;
                }
            }else{
                $cartItems[] = $this->cartItemArray($request->quantity, $product);
            }

            Session::put('cartItems', $cartItems);
            Session::save();
        }

        return response()->json([
            'success' => true,
            'total' => 99.00,
            'counter' => isset($cart->items) ? $cart->items->count() : count($cartItems),
        ], 200);

    }

    /**
     * @param $quantity
     * @param $product
     * @return array
     */
    public function cartItemArray($quantity, $product){
        return array(
                    'product_id' => $product->id,
                    'price' => $product->price,
                    'quantity' => $quantity,
                    'total' => ($product->price * $quantity)
                );
    }

    /**
     * Checkout page
     * @return Application|Factory|View
     */
    public function checkout(){
        $user = Auth::user();
        $cart = $this->cart->where('user_id', $user->id)->first();
        $paymentMethods = PaymentMethod::all();
        $addresses = $user->addresses;
        return view('checkout', compact('cart', 'paymentMethods', 'addresses'));
    }

    /**
     * Get cart session
     * @return mixed
     */
    public function getCartSession() {
        return Session::get('cartItems');
    }

    /**
     *  Get Delivery Fee
     * @return float
     */
    public function getDeliveryFee(){
        return 50.00;
    }

    /**
     * Convert Items array into items collection
     * @return Collection
     */
    public function formatCartItems(){
        $cartItems = $this->getCartSession();
        $items = collect();
        foreach ($cartItems as $cartItem){
            $item = new CartItem();
            $item->cart_id = 0;
            $item->product_id = $cartItem['product_id'];
            $item->quantity = $cartItem['quantity'];
            $item->price = $cartItem['price'];
            $item->total = $cartItem['total'];
            $items->add($item);
        }

        return $items;
    }

    /**
     * Calculate items total
     * @return int
     */
    public function cartItemsTotal(){
        $items = $this->formatCartItems();
        $total = 0;
        foreach ($items as $item){
            $total += $item->total;
        }

        return $total;
    }

    /**
     * Calculate items total
     * @return void
     */
    public function saveSessionItemsToCart(){
        $cartItems = $this->getCartSession();
        $cart = $this->cart->where('user_id', Auth::id())->first();

        if($cart == null){
            $cart = $this->cart;
            $cart->user_id = Auth::id();
            $cart->vat = 0;
            $cart->total = 0;
            $cart->items_total = 0;
            $cart->delivery_fee = $this->getDeliveryFee();
            $cart->save();
        }

        foreach ($cartItems as $cartItem){
            $item = $this->item;
            $item->cart_id = $cart->id;
            $item->product_id = $cartItem['product_id'];
            $item->quantity = $cartItem['quantity'];
            $item->price = $cartItem['price'];
            $item->total = $cartItem['total'];
            $item->save();
        }
        $this->updateCart();
    }

    /**
     * Update Cart Totals
     */
    public function updateCart(){
        $cart = $this->cart->where('user_id', Auth::id())->first();
        $itemsTotal = $this->itemsTotal($cart);
        $cart->items_total = $itemsTotal;
        $cart->total = ($itemsTotal + $cart->delivery_fee + $cart->vat);
        $cart->save();
    }

    /**
     * Calculate Items total
     * @param $cart
     * @return float|int
     */
    public function itemsTotal($cart){
        $itemsTotal = 0;
        foreach ($cart->items as $item){
            if($item->total != ($item->quantity * $item->price)) {
                $item->total = ($item->quantity * $item->price); //Just to make sure all is correct
                $item->save();
            }
            $itemsTotal += $item->total;
        }
        return $itemsTotal;
    }

    public function deleteItem(Request $request){
        $id = $this->hashids->decode($request->id)[0];
        $product = $this->product->where('id', $id)->first();
        if(Auth::check()){
            $this->item->where('product_id', $product->id)->delete();
            $this->updateCart();
        } else{

        }
        return response()->json([
            'success' => true,
            'message' => 'Cart item deleted successfully!'
        ], 200);
    }
}
