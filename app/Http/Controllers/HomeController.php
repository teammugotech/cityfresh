<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use Hashids\Hashids;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $hashids;
    protected $product;
    protected $category;

    /**
     * Create a new controller instance.
     *
     * @param Hashids $hashids
     * @param Product $product
     * @param ProductCategory $category
     */
    public function __construct(Hashids $hashids, Product $product, ProductCategory  $category){
        $this->hashids = $hashids;
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $products = $this->product->all();
        $categories = $this->category->all();
        return view('home', compact('categories', 'products'));
    }
}
