<?php

namespace App\Http\Controllers;

use App\Order;
use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use App\Payment;
use Illuminate\Http\Request;
use Billow\Contracts\PaymentProcessor;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class PaymentController extends Controller
{
    protected $payfast;
    public function __construct(PaymentProcessor $payfast){
        $this->payfast = $payfast;

    }

    /**
     * @return mixed
     */
    public function confirmPayment($id) {

        $order = Order::find($id);
        $date = Carbon::now();

        $payment = Payment::create([
            'user_id' => Auth::id(),
            'm_payment_id' => $date,
            'order_id' => $order->id,
            'amount' => $order->total,
            'description' => 'City Fresh Online Purchase'
        ]);

        // Build up payment Paramaters.
        $this->payfast->setBuyer($payment->user()->name, $payment->user()->surname, $payment->user()->email);
        $this->payfast->setAmount($payment->amount);
        $this->payfast->setItem(Config::get('app.name') . ' Purchase', $payment->description);
        $this->payfast->setMerchantReference($payment->m_payment_id);

        // Return the payment form.
        return $this->payfast->paymentForm('Pay now');
    }


    /**
     * Store payment entry into the DB
     * @param Request $request
     */
    public function store(Request $request){
        //Generate unique m_payment_id
        $payment = new Payment();
        $payment->save();
    }


    /**
     * @return Application|Factory|View
     */
    public function success(){
        $payment = Payment::where('user_id', Auth::id())
            ->with('status')
            ->whereNotNull('payment_status_id')
            ->orderBy('id', 'DESC')
            ->first();
        return view('payments.success', ['payment' => $payment]);
    }


    /**
     * @return Application|Factory|View
     */
    public function cancel(){
        $payment = Payment::where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->first();
        $payfast = $this->payfastForm($payment);
        return view('payments.cancel', ['payfast' => $payfast, 'payment' => $payment]);
    }

    /**
     * @param Request $request
     */
    public function itn(Request $request){
        // Retrieve the payment from persistence. Eloquent Example.
        $payment = Payment::where('m_payment_id', $request->get('m_payment_id'))->firstOrFail();

        // Verify the payment status.
        $status = $this->payfast->verify($request, $payment->amount)->status();

        // Handle the result of the transaction.
        switch($status)
        {
            case 'COMPLETE':
                // Things went as planned, update your payment status and notify the customer/admins.
                $payment->update([
                    'payment_status_id' => 2,
                    'pf_payment_id' => $request->pf_payment_id
                ]);
                break;
            case 'FAILED':
                // We've got problems, notify admin and contact Payfast Support.
                $payment->update([
                    'payment_status_id' => 3,
                    'pf_payment_id' => $request->pf_payment_id
                ]);
                break;
            case 'PENDING':
                // We've got problems, notify admin and contact Payfast Support.
                $payment->update([
                    'payment_status_id' => 1,
                    'pf_payment_id' => $request->pf_payment_id
                ]);
                break;
            default:
                // We've got problems, notify admin to check logs.
                $payment->update([
                    'payment_status_id' => 4,
                    'pf_payment_id' => $request->pf_payment_id
                ]);
                break;
        }
    }

    /**
     * @param $payment
     * @return mixed
     */
    public function payfastForm($payment){
        $this->payfast->setBuyer($payment->user()->name, $payment->user()->surname, $payment->user()->email);
        $this->payfast->setAmount($payment->amount);
        $this->payfast->setItem(Config::get('app.name') . ' Purchase', $payment->description);
        $this->payfast->setMerchantReference($payment->m_payment_id);

        // Return the payment form.
        return $this->payfast->paymentForm('Retry Payment');
    }
}
