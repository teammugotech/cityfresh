<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CartItem extends Model
{
    protected $table = 'cart_items';
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }

}
