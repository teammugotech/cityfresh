<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    protected $table = 'payments';
    protected $guarded = [];

    /**
     * @return BelongsTo
     */
    public function status(){
        return $this->belongsTo(PaymentStatus::class, 'payment_status_id');
    }

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return BelongsTo
     */
    public function order(){
        return $this->belongsTo(Order::class, 'order_id');
    }
}
