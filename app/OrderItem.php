<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $table = 'order_items';
    protected $dates = ['deleted_at'];

    /**
     * @return BelongsTo
     */
    public function product(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
